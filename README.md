# js-tool

- > js 工具集
- > 仓库地址：https://gitee.com/li_shifeng/js-tool

## 使用方法

1. 安装

```bash
npm i @jstool100/jstool
```

2. 使用

```javascript
import { getUrlValue } from '@jstool100/jstool'

getUrlValue('key') // => value
```
