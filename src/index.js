import { getUrlValue, isMobile, isWx, isPhoneNum, isIdentifyCode } from './utils/common/index'
import storage from './utils/common/storage'
import date from './utils/common/date'

export { getUrlValue, isMobile, isWx, isPhoneNum, isIdentifyCode, storage, date }
