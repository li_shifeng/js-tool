const storage = {
  storage: window.localStorage,
  session: {
    storage: window.sessionStorage
  }
}

const methods = {
  set(key, value) {
    if (typeof value === 'string') {
      this.storage.setItem(key, value)
    } else {
      this.storage.setItem(key, JSON.stringify(value))
    }
  },
  get(key) {
    let value = this.storage.getItem(key)
    try {
      value = JSON.parse(value)
    } catch (error) {}
    return value
  },
  remove(key) {
    this.storage.removeItem(key)
  },
  clear() {
    this.storage.clear()
  }
}

Object.assign(storage, methods)
Object.assign(storage.session, methods)

export default storage
