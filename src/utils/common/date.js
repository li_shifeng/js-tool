// 日期时间相关方法

const addZero = num => (num < 10 ? `0${num}` : num)

const weekTextMap = {
  style1: [
    '\u661f\u671f\u65e5',
    '\u661f\u671f\u4e00',
    '\u661f\u671f\u4e8c',
    '\u661f\u671f\u4e09',
    '\u661f\u671f\u56db',
    '\u661f\u671f\u4e94',
    '\u661f\u671f\u516d'
  ],
  // style1: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
  style2: ['\u5468\u65e5', '\u5468\u4e00', '\u5468\u4e8c', '\u5468\u4e09', '\u5468\u56db', '\u5468\u4e94', '\u5468\u516d']
  // style2: ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
}

const formatDate = (date, format) => {
  const formatConfig = {
    YYYY: date.getFullYear(), // 年(4位)
    YY: date.getFullYear().toString().substr(2), // 年(2位)
    MM: addZero(date.getMonth() + 1), // 月(小于10，前补0)
    M: date.getMonth() + 1, // 月(小于10，前不补0)
    DD: addZero(date.getDate()), // 日(小于10，前补0)
    D: date.getDate(), // 日(小于10，前不补0)
    HH: addZero(date.getHours()), // 小时(24小时制，小于10，前补0)
    H: date.getHours(), // 小时(24小时制，小于10，前不补0)
    hh: addZero(date.getHours() > 12 ? date.getHours() - 12 : date.getHours()), // 小时(12小时制，小于10，前补0)
    h: date.getHours() > 12 ? date.getHours() - 12 : date.getHours(), // 小时(12小时制，小于10，前不补0)
    mm: addZero(date.getMinutes()), // 分钟(小于10，前补0)
    m: date.getMinutes(), // 分钟(小于10，前不补0)
    ss: addZero(date.getSeconds()), // 秒(小于10，前补0)
    s: date.getSeconds(), // 秒(小于10，前不补0)
    WWW: weekTextMap.style1[date.getDay()], // 星期(例：星期一)
    WW: weekTextMap.style2[date.getDay()], // 星期(例：周一)
    W: date.getDay(), // 星期(例：1)
    A: date.getHours() < 12 ? '\u4e0a\u5348' : '\u4e0b\u5348' // 上午/下午
    // A: date.getHours() < 12 ? '上午' : '下午' // 上午/下午
  }

  let res = format
  for (let key in formatConfig) {
    if (new RegExp(`(${key})`).test(format)) {
      res = res.replace(RegExp.$1, formatConfig[key])
    }
  }
  return res
}

const date = d => {
  const date = d ? new Date(d) : new Date()
  return {
    format(f = 'YYYY-MM-DD HH:mm:ss') {
      return formatDate(date, f || 'YYYY-MM-DD HH:mm:ss') // 兼容 format('') 传了空字符串的情况
    }
  }
}

export default date
